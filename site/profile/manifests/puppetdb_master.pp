#
# profile::puppetdb_master
#

class profile::puppetdb_master {


# Hiera PuppetDB

  $dbusername = lookup('puppetdb_master::dbusername')
  $dbpassword = lookup('puppetdb_master::dbpassword')


# PuppetDB
  class { 'puppetdb':

      # Basic DB configuration:
      # Using the same database for both read/write
      # Remember to change these values in a production setup!

      database_name     => 'puppetdb',
      database_username => $dbusername,
      database_password => $dbpassword,
  }

# Running PuppetDB in single-node mode, i.e. PuppetDB-Master
  class { 'puppetdb::master::config':

    manage_routes           => true,
    manage_storeconfigs     => true,
    manage_report_processor => false,
    manage_config           => true,

  }

}

