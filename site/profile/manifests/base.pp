class profile::base {

    unless $facts['hostname'] =~ /^(?i)(splunk[0-9]+|test)$/ {  # prod-splunk and test-splunk
        include profile::splunk::forwarder
    }

    case $facts['os']['name'] {
        'Windows': { contain profile::base_windows }
        default:   { contain profile::base_linux   }
    }

}
