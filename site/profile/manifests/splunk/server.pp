class profile::splunk::server {

    @splunk_input { 'default-splunktcp':
        section => 'splunktcp://9997',
        setting => 'disable',
        value   => '0',
    }

    @splunk_input { 'ossec-syslog':
        section => 'udp://0.0.0.0:514',
        setting => 'disable',
        value   => '0',
    }


    @splunk_input { '/var/log':
        section => 'monitor:///var/log',
        setting => 'disable',
        value   => '0',
    }

    # The Splunk puppet module always runs the license_splunkforwarder and enable_splunkforwarder
    # directives even though we haven't installed the splunk forwarder here.
    # Workaround for now:
    if $::osfamily == 'Windows' {
        $server_dir = 'C:/Program Files/Splunk'
        $user = 'Administrator'
    } else {
        $server_dir = '/opt/splunk'
        $user = 'splunk'
    }

    class { '::splunk::params':
        forwarder_installdir => $server_dir,
    }

    class { '::splunk':
        splunkd_listen => '0.0.0.0',
        splunk_user    => $user,
        purge_inputs   => true,
        purge_outputs  => true,
    }

}
