class profile::splunk::forwarder {

    if $::osfamily != 'Windows' {
        @splunkforwarder_input { '/var/log':
            section => 'monitor:///var/log',
            setting => 'disable',
            value   => '0',
        }
    }


    # The Splunk puppet module always runs the license_splunk and
    # enable_splunk directives even though we haven't installed the
    # splunk server here.
    # Workaround for now:
    if $::osfamily == 'Windows' {
        $server_dir = 'C:/Program Files/SplunkUniversalForwarder'
        # FIXME: Should be changed to a service account
        $user = 'Administrator'
    } else {
        $server_dir = '/opt/splunkforwarder'
        $user = 'splunk'
    }

    class { 'splunk::params':
        server            => 'splunk0.borg.trek',
        # Workaround
        server_installdir => $server_dir,
    }

    class { 'splunk::forwarder':
        splunk_user   => $user,

        purge_inputs  => true,
        purge_outputs => true,
    }
}
