# profile::base_windows
#

class profile::base_windows {


# OSSEC-client: 
  unless $facts['hostname'] =~ /^(?i)(hids)(\d){1,2}$/ {
  # Unless OSSEC-server, i.e. hostname doesn't match hidsX-hidsXX

    include ossec::client
    # See Hiera for variables.
  }

# use PowerShell DSC to set timezone
  dsc_xtimezone { 'Oslo':
    dsc_timezone         => 'W. Europe Standard Time',
    dsc_issingleinstance => 'yes',
  }

}
