# Profile ossec_hids_server

class profile::ossec_hids_server {

# OSSEC-server
# See Hiera for variables


  class { 'ossec::server':
    syslog_output        => true,
    syslog_output_format => 'splunk',
  }

  ossec::addlog { 'monitorLogFile':
    logfile => '/var/log/secure',
    logtype => 'syslog'
  }


  @splunkforwarder_input { '/var/ossec/logs/alerts':
    section => 'monitor:///var/ossec/logs/alerts/alerts*',
    setting => 'sourcetype',
    value   => 'ossec_alerts',
  }

  @splunkforwarder_input { '/var/ossec/logs/ossec.log':
    section => 'monitor:///var/ossec/logs/ossec.log',
    setting => 'sourcetype',
    value   => 'ossec_log',
  }

  @splunkforwarder_input { '/var/ossec/logs/firewall':
    section => 'monitor:///var/ossec/logs/firewall/firewall.log',
    setting => 'sourcetype',
    value   => 'ossec_alerts',
  }

  @splunkforwarder_input { '/var/ossec/logs/active-response.log':
    section => 'monitor:///var/ossec/logs/active-response.log',
    setting => 'sourcetype',
    value   => 'ossec_ar',
  }

  user { 'splunk':
    ensure => 'present',
    groups => ['splunk','ossec'],
  }

}
