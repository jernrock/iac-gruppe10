class role::splunk_server {

    include profile::splunk::server

    include ossecced::splunk_server

}
