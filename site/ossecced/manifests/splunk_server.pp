class ossecced::splunk_server {

    archive { '/tmp/ossec-splunk-app.tar.gz':
        extract       => true,
        extract_path  => '/opt/splunk/etc/apps',
        creates       => '/opt/splunk/etc/apps/ossec',

        source        => 'https://flymr.liquidcode.net/bb8803fac5cadc240031a9f34af21449ebc21340.gzip',
        checksum      => 'bb8803fac5cadc240031a9f34af21449ebc21340',
        checksum_type => 'sha1',

        require       => Class['::splunk'],
        notify        => Service['splunk'],
    }

}
