forge "http://forge.puppetlabs.com"

# Modules from the Puppet Forge
# Versions should be updated to be the latest at the time you start
#mod "puppetlabs/inifile",     '1.5.0'
#mod "puppetlabs/stdlib",      '4.11.0'
#mod "puppetlabs/concat",      '2.1.0'

# Modules from Git
# Examples: https://github.com/puppetlabs/r10k/blob/master/doc/puppetfile.mkd#examples
#mod 'apache',
#  :git    => 'https://github.com/puppetlabs/puppetlabs-apache',
#  :commit => '83401079053dca11d61945bd9beef9ecf7576cbf'

#mod 'apache',
#  :git    => 'https://github.com/puppetlabs/puppetlabs-apache',
#  :branch => 'docs_experiment'

# Lib
mod 'puppetlabs/concat', '2.2.1'
mod 'puppetlabs/stdlib', '4.17.1'
mod 'puppetlabs/firewall', '1.9.0'
mod 'lwf/remote_file', '1.1.3'
mod 'stm/debconf', '2.0.0'

mod 'puppetlabs/ntp', '6.2.0'

mod 'puppetlabs/puppetdb', '6.0.1'
mod 'puppetlabs/postgresql', '5.1.0'

mod 'puppetlabs/dsc', '1.3.0'
mod 'puppetlabs/reboot', '1.2.1'

mod 'puppetlabs/chocolatey', '3.0.0'
mod 'puppetlabs/powershell', '2.1.1'
mod 'puppetlabs-registry', '1.1.4'

mod 'puppet/r10k', '6.1.0'
mod 'puppetlabs/ruby', '1.0.0'
mod 'puppetlabs/gcc', '0.3.0'
mod 'puppet/make', '1.1.0'
mod 'puppetlabs/inifile', '1.6.0'
mod 'puppetlabs/vcsrepo', '1.5.0'
mod 'puppetlabs/git', '0.5.0'

mod 'gentoo/portage', '2.3.0'

mod 'saz/timezone', '3.5.0'

# to set dhcp on second interface on Ubuntu
mod 'puppet-network', '0.8.0'
mod 'puppet-filemapper', '2.1.0'
mod 'puppet-boolean', '1.1.0'
mod 'camptocamp-kmod', '2.1.0'

# fix routing with two NICs
mod 'example42-network', '3.3.8'

# ELK:
mod 'pcfens/filebeat', '0.11.2'
mod 'elastic/logstash', '5.2.1'
mod 'elastic/elasticsearch', '5.2.0'
mod 'elastic/kibana', '5.0.0'
mod 'puppetlabs/java', '1.6.0'
mod 'puppet/archive', '1.3.0'
mod 'puppet/nginx', '0.6.0'
mod 'maestrodev/wget', '1.7.3'
mod 'leinaddm/htpasswd', '0.0.3'
mod 'puppet/yum', '1.0.0'
mod 'richardc/datacat', '0.6.2'


# Sensu
mod 'sensu/sensu', '2.27.0'
mod 'puppetlabs/rabbitmq', '5.6.0'
mod 'puppet/staging', '1.0.6'
mod 'yelp/uchiwa', '2.0.0'
mod 'puppetlabs/apt', '3.0.0'

mod 'arioch/redis', '1.2.4'
mod 'stahnma/epel', '1.2.2'

# Metrics - graphite/grafana
mod 'dwerder/graphite', '7.0.0'
mod 'puppet/grafana', '3.0.0'
mod 'garethr-docker', '5.3.0'

# fun
mod 'devsec-suricata', '0.1.1'

# Project
mod 'wazuh-ossec', '2.0.21'
mod 'puppet-splunk', '7.0.0'
