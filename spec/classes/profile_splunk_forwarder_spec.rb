require 'spec_helper'
require 'pp'

describe 'profile::splunk::forwarder', :type => :class do
    params = {
        "ubuntu" => {
            :user         => "splunk",
            :path         => "/opt/splunkforwarder",
        },
        "windows" => {
            :user         => "Administrator",
            :path         => "C:/Program Files/SplunkUniversalForwarder",
        },
    }

    test_on = {
        :hardwaremodels => [ 'x86_64', 'x64' ],
        :supported_os => [
            { 'operatingsystem' => 'Ubuntu', 'operatingsystemrelease' => [ '16.04' ] },
            { 'operatingsystem' => 'windows', 'operatingsystemrelease' => [ '2012' ] },
        ]
    }

    on_supported_os(test_on).each do |os, facts|

        context "#{os}" do
            let (:facts) { facts }

            os_name = os.split("-")[0]

            # Attempt to work around missing Puppet::Util::Windows initialization
            # for the first Windows test run
            it "Empty test" do
                begin
                    is_expected.to contain_class('Splunk::Params')
                rescue Puppet::PreformattedError

                end
            end

            it "sets splunk server path (workaround for module bug)" do
                path = params[os_name][:path]

                is_expected.to contain_class('Splunk::Params').with({
                    # We do not test the forwarder dir here as we use the
                    # default instead of overriding it.
                    :server_installdir    => path,
                })
            end

            it "sets splunk user" do
                is_expected.to contain_class('Splunk::Forwarder').with({
                    :splunk_user => params[os_name][:user],
                })
            end

        end

    end
end
