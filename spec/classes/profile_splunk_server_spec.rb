require 'spec_helper'
require 'pp'

describe 'profile::splunk::server', :type => :class do
    params = {
        "ubuntu" => {
            :user         => "splunk",
            :path         => "/opt/splunk",
        },
        "windows" => {
            :user         => "Administrator",
            :path         => "C:/Program Files/Splunk",
        },
    }

    test_on = {
        :hardwaremodels => [ 'x86_64', 'x64' ],
        :supported_os => [
            { 'operatingsystem' => 'Ubuntu', 'operatingsystemrelease' => [ '16.04' ] },
            { 'operatingsystem' => 'windows', 'operatingsystemrelease' => [ '2012' ] },
        ]
    }

    on_supported_os(test_on).each do |os, facts|

        context "#{os}" do
            let (:facts) { facts }

            os_name = os.split("-")[0]

            it "sets splunk forwarder path" do
                path = params[os_name][:path]

                is_expected.to contain_class('Splunk::Params').with({
                    :forwarder_installdir    => path,
                })
            end

            it "sets splunk user" do
                is_expected.to contain_class('splunk').with({
                    :splunk_user => params[os_name][:user],
                })
            end


        end

    end
end
