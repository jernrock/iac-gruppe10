import sys
import socket
host = 'irc.chatno.de'
port = 6667
channel = '#iac'
msg = " ".join(sys.argv[1:])

join_sock = socket.socket()
join_sock.connect((host, port))

join_sock.send("NICK Jenkins\r\nUSER skyhigh x 8 :skyhigh\r\n")

buffer = ""
while "PING" not in buffer:
    buffer += join_sock.recv(1024)

print(buffer)
lines = buffer.split("\r\n")
for line in lines:
    if line.startswith("PING"):
        print("Responding to ping with %s" % line.split(" ")[-1])
        join_sock.send("PONG " + line.split(" ")[-1] + "\r\n")
        break

buffer = ""
while "Welcome to the ChatNode! IRC Network" not in buffer:
    buffer += join_sock.recv(1024)
print(buffer)

join_sock.send("JOIN :" + channel + "\r\n"
               "PRIVMSG " + channel + " :" + msg + "\r\n"
               "QUIT :Post-build action complete\r\n")

buffer = ""
while "QUIT" not in buffer and "ERROR" not in buffer:
    buffer += join_sock.recv(1024)
print(buffer)
